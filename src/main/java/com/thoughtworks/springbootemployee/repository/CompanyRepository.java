package com.thoughtworks.springbootemployee.repository;

import com.thoughtworks.springbootemployee.entity.Company;
import com.thoughtworks.springbootemployee.entity.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Repository
public class CompanyRepository {

    @Autowired
    private EmployeeRepository employeeRepository;


    private final List<Company> companies = new ArrayList<>();


    public List<Company> getCompanies() {
        return companies;
    }


    public Company getCompanyById(Long companyId) {
        return companies
                .stream()
                .filter(company -> Objects.equals(companyId, company.getId()))
                .findFirst().orElse(null);
    }


    public void deleteCompanyById(Long id) {
        companies.remove(getCompanyById(id));
    }

    public Company updateCompanyName(Long id, Company company) {
        Company updateCompany = getCompanyById(id);
        updateCompany.setName(company.getName());
        return updateCompany;
    }

    public Company addCompany(Company company) {
        company.setId(generateId());
        companies.add(company);
        return company;
    }

    public Long generateId() {
        return companies
                .stream()
                .max(Comparator.comparingLong(Company::getId))
                .map(employee -> employee.getId() + 1)
                .orElse(1L);
    }

    public List<Company> getCompaniesByPage(Integer page, Integer size) {
        return companies.stream()
                .skip((long) (page - 1) * size)
                .limit(size)
                .collect(Collectors.toList());
    }

    public List<Employee> getEmployeesByCompanyId(Long companyId) {
        List<Employee> employees = employeeRepository.geEmployees();
        return employees.stream().filter(employee -> Objects.equals(employee.getCompanyId(), companyId)).collect(Collectors.toList());
    }
}
