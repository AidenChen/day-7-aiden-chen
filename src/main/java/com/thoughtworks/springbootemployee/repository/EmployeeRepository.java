package com.thoughtworks.springbootemployee.repository;

import com.thoughtworks.springbootemployee.entity.Employee;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Repository
public class EmployeeRepository {


    private final List<Employee> employees = new ArrayList<>();

    public Employee save(Employee employee) {
        employee.setId(generateId());
        employees.add(employee);
        return employee;
    }


    public Long generateId() {
        return employees
                .stream()
                .max(Comparator.comparingLong(Employee::getId))
                .map(employee -> employee.getId() + 1)
                .orElse(1L);
    }

    public List<Employee> getEmployeesByGender(String gender) {
        return employees
                .stream()
                .filter(employee -> employee.getGender().equals(gender))
                .collect(Collectors.toList());
    }

    public Employee getEmployeeById(Long id) {
        return employees.stream().filter(employee -> Objects.equals(employee.getId(), id)).findFirst().orElse(null);
    }

    public List<Employee> geEmployees() {
        return employees;
    }

    public void deleteEmployeeById(Long id) {
        employees.remove(getEmployeeById(id));
    }

    public List<Employee> getEmployeesByPage(Integer page, Integer size) {
        return employees.stream()
                .skip((long) (page - 1) * size)
                .limit(size)
                .collect(Collectors.toList());
    }

    public Employee updateEmployeeByCondition(Long id, Employee employee) {
        Employee updateEmployee = getEmployeeById(id);
        updateEmployee.setAge(employee.getAge());
        updateEmployee.setSalary(employee.getSalary());
        updateEmployee.setCompanyId(employee.getCompanyId());
        return updateEmployee;
    }


}
